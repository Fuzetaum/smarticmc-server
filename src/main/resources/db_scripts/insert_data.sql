INSERT INTO user (username, password, name, email)
	VALUES ('smarticmc.admin', 'smarticmc.admin', 'SmartICMC Administrator', 'a@a.com');

INSERT INTO media (type, value)
	VALUES ('image', 'https://i.ibb.co/t3YfmqH/Pau-Brasil.jpg');
INSERT INTO media (type, value)
	VALUES ('text', 'Planta Nativa - Com a promulgação da Lei 6.607/78, o pau-brasil tornou-se a Árvore Nacional e o dia 3 de maio a sua data comemorativa.');
INSERT INTO media (type, value)
	VALUES ('image', 'https://i.ibb.co/G22M9Wj/Canela.jpg');
INSERT INTO media (type, value)
	VALUES ('text', 'Planta Exótica – Considerada símbolo da sabedoria, a canela foi usda na Antiguidade pelos gregos, romanos, hebreus, além de ter sido a especiaria mais procurada na Europa e seu comércio, muito lucrativo. Extraída do tronco, a canela é utilizada para fins culinários, cosméticos e medicinais.');
INSERT INTO media (type, value)
	VALUES ('image', 'https://i.ibb.co/0F0gYm2/Murta.jpg');
INSERT INTO media (type, value)
	VALUES ('text', 'Planta Nativa - Conhecida também como chal-chal ou fruta-do-pombo. Sua floração ocorre entre novembro e dezembro e atrai aves como sabiás, sanhaçus, bem-tevi, trinca-ferro, entre outros.');
INSERT INTO media (type, value)
	VALUES ('image', 'https://i.ibb.co/ZKKhgGH/Areca-Bambu.jpg');
INSERT INTO media (type, value)
	VALUES ('text', 'Planta Exótica - Sua origem é africana, da ilha de Madagascar. Apresenta um rápido crescimento e é muito utilizada para ornamentação.');
INSERT INTO media (type, value)
	VALUES ('image', 'https://i.ibb.co/r7jQMcj/Sibipiruna.jpg');
INSERT INTO media (type, value)
	VALUES ('text', 'Planta nativa – Essa espécie pode atingir até 28 mestros de altura. Muito confundida com pau-brasil ou pau-ferro pela semelhança da folhagem. É amplamente utilizada na arborização de cidades.');
INSERT INTO media (type, value)
	VALUES ('image', 'https://i.ibb.co/KD0dWrh/Cabreuva.jpg');
INSERT INTO media (type, value)
	VALUES ('text', 'Planta Nativa - Nativa das regiões sul, sudeste e nordeste do Brasil. Essa espécie pode atingir até 30 metros de altura. Sua madeira nobre e sua serragem são utilizados na indútria de perfumaria.');
INSERT INTO media (type, value)
	VALUES ('image', 'https://i.ibb.co/VwmhydC/Pinheiro.jpg');
INSERT INTO media (type, value)
	VALUES ('text', 'Planta Exótica - Nativos do hemisfério norte. Ocorre desde o México, Califórnia, passando pela Europa e Norte da África até o Japão.');
INSERT INTO media (type, value)
	VALUES ('image', 'https://i.ibb.co/41csVWH/Espatodea.jpg');
INSERT INTO media (type, value)
	VALUES ('text', 'Planta Exótica - Nativa da áfrica tropical, atinge até 25 metros de altura. Utilizada geralmente como planta ornamental. Suas flores avermelhadas são muito apreciadas.');
INSERT INTO media (type, value)
	VALUES ('image', 'https://i.ibb.co/kHVDNGr/Quaresmeira.jpg');
INSERT INTO media (type, value)
	VALUES ('text', 'Planta Nativa – É uma árvore brasileira, da Mata Atlântica. Seu nome popular é devido à cor das flores (roxas) e época de floração: entre os meses de janeiro e abril.');


INSERT INTO item (name, description)
	VALUES ('Pau-Brasil', 2);
INSERT INTO item (name, description)
	VALUES ('Canela', 4);
INSERT INTO item (name, description)
	VALUES ('Murta', 6);
INSERT INTO item (name, description)
	VALUES ('Areca-Bambu', 8);
INSERT INTO item (name, description)
	VALUES ('Sibipiruna', 10);
INSERT INTO item (name, description)
	VALUES ('Cabreúva', 12);
INSERT INTO item (name, description)
	VALUES ('Pinheiro', 14);
INSERT INTO item (name, description)
	VALUES ('Espatódea', 16);
INSERT INTO item (name, description)
	VALUES ('Quaresmeira', 18);

INSERT INTO item_media (item_id, media_id)
	VALUES (1, 1);
INSERT INTO item_media (item_id, media_id)
	VALUES (2, 3);
INSERT INTO item_media (item_id, media_id)
	VALUES (3, 5);
INSERT INTO item_media (item_id, media_id)
	VALUES (4, 7);
INSERT INTO item_media (item_id, media_id)
	VALUES (5, 9);
INSERT INTO item_media (item_id, media_id)
	VALUES (6, 11);
INSERT INTO item_media (item_id, media_id)
	VALUES (7, 13);
INSERT INTO item_media (item_id, media_id)
	VALUES (8, 15);
INSERT INTO item_media (item_id, media_id)
	VALUES (9, 17);

INSERT INTO map_node (latitude, longitude, is_item)
	VALUES (-22.007741, -47.895218, FALSE);
INSERT INTO map_node (latitude, longitude, is_item)
	VALUES (-22.0077, -47.894598, FALSE);
INSERT INTO map_node (latitude, longitude, is_item)
	VALUES (-22.007684, -47.894898, FALSE);
INSERT INTO map_node (latitude, longitude, is_item)
	VALUES (-22.007493, -47.895224, FALSE);
INSERT INTO map_node (latitude, longitude, is_item)
	VALUES (-22.007383, -47.894637, FALSE);
INSERT INTO map_node (latitude, longitude, is_item)
	VALUES (-22.007396, -47.894961, FALSE);
INSERT INTO map_node (latitude, longitude, is_item)
	VALUES (-22.007294, -47.895207, FALSE);
INSERT INTO map_node (latitude, longitude, is_item)
	VALUES (-22.007179, -47.894541, FALSE);
INSERT INTO map_node (latitude, longitude, is_item)
	VALUES (-22.006998, -47.895244, FALSE);
INSERT INTO map_node (latitude, longitude, is_item)
	VALUES (-22.006957, -47.894492, FALSE);
INSERT INTO map_node (latitude, longitude, is_item)
	VALUES (-22.006467, -47.895113, FALSE);

/*Pau-Brasil*/
INSERT INTO map_node (latitude, longitude, is_item)
	VALUES (-22.007040, -47.894193, TRUE);
/*Canela*/
INSERT INTO map_node (latitude, longitude, is_item)
	VALUES (-22.006899, -47.894481, TRUE);
/*Murta*/
INSERT INTO map_node (latitude, longitude, is_item)
	VALUES (-22.007170, -47.894583, TRUE);
/*Areca-Bambu*/
INSERT INTO map_node (latitude, longitude, is_item)
	VALUES (-22.006817, -47.895297, TRUE);
/*Sibipiruna*/
INSERT INTO map_node (latitude, longitude, is_item)
	VALUES (-22.007710, -47.893930, TRUE);
/*Cabreúva*/
INSERT INTO map_node (latitude, longitude, is_item)
	VALUES (-22.006952, -47.894578, TRUE);
/*Pinheiro*/
INSERT INTO map_node (latitude, longitude, is_item)
	VALUES (-22.006998, -47.895201, TRUE);
/*Espatódea*/
INSERT INTO map_node (latitude, longitude, is_item)
	VALUES (-22.007891, -47.895158, TRUE);
/*Quaresmeira*/
INSERT INTO map_node (latitude, longitude, is_item)
	VALUES (-22.006992, -47.895548, TRUE);
	
INSERT INTO map_path (edge_1, edge_2)
	VALUES (1, 3);
INSERT INTO map_path (edge_1, edge_2)
	VALUES (1, 4);
INSERT INTO map_path (edge_1, edge_2)
	VALUES (2, 3);
INSERT INTO map_path (edge_1, edge_2)
	VALUES (2, 5);
INSERT INTO map_path (edge_1, edge_2)
	VALUES (3, 1);
INSERT INTO map_path (edge_1, edge_2)
	VALUES (3, 2);
INSERT INTO map_path (edge_1, edge_2)
	VALUES (3, 6);
INSERT INTO map_path (edge_1, edge_2)
	VALUES (4, 1);
INSERT INTO map_path (edge_1, edge_2)
	VALUES (4, 6);
INSERT INTO map_path (edge_1, edge_2)
	VALUES (4, 7);
INSERT INTO map_path (edge_1, edge_2)
	VALUES (5, 2);
INSERT INTO map_path (edge_1, edge_2)
	VALUES (5, 6);
INSERT INTO map_path (edge_1, edge_2)
	VALUES (5, 8);
INSERT INTO map_path (edge_1, edge_2)
	VALUES (6, 3);
INSERT INTO map_path (edge_1, edge_2)
	VALUES (6, 4);
INSERT INTO map_path (edge_1, edge_2)
	VALUES (6, 5);
INSERT INTO map_path (edge_1, edge_2)
	VALUES (6, 7);
INSERT INTO map_path (edge_1, edge_2)
	VALUES (7, 4);
INSERT INTO map_path (edge_1, edge_2)
	VALUES (7, 6);
INSERT INTO map_path (edge_1, edge_2)
	VALUES (7, 9);
INSERT INTO map_path (edge_1, edge_2)
	VALUES (8, 5);
INSERT INTO map_path (edge_1, edge_2)
	VALUES (8, 10);
INSERT INTO map_path (edge_1, edge_2)
	VALUES (9, 7);
INSERT INTO map_path (edge_1, edge_2)
	VALUES (9, 11);
INSERT INTO map_path (edge_1, edge_2)
	VALUES (10, 8);
INSERT INTO map_path (edge_1, edge_2)
	VALUES (10, 11);
INSERT INTO map_path (edge_1, edge_2)
	VALUES (11, 9);
INSERT INTO map_path (edge_1, edge_2)
	VALUES (11, 10);
