package edu.usp.smarticmc.provedorDeConteudo;

import java.util.List;
import java.util.stream.Collectors;

import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import edu.usp.smarticmc.Features;
import edu.usp.smarticmc.jooq.tables.daos.MediaDao;
import edu.usp.smarticmc.jooq.tables.pojos.Item;
import edu.usp.smarticmc.jooq.tables.pojos.Media;

import static edu.usp.smarticmc.jooq.tables.Interest.INTEREST;
import static edu.usp.smarticmc.jooq.tables.ItemMedia.ITEM_MEDIA;

@Component("contentProviderItemDao")
public class ItemDao {
	@Autowired
	private edu.usp.smarticmc.jooq.tables.daos.ItemDao jooqItemDao;
	
	@Autowired
	private MediaDao jooqMediaDao;
	
	@Autowired
	private DSLContext dslContext;
	
	public List<ItemDto> getItemList() {
//		return this.dslContext
//				.selectFrom(ITEM)
//				.fetch()
//				.stream()
//				.map(item -> new ItemDto(item.getName(), item.getDescription(), item.getType(), item.getUrl(), item.getAmountOfLikes()))
//				.collect(Collectors.toList());
		return null;
	}

	public ItemDto getItem(Integer id) {
		Item item = jooqItemDao.fetchById(id).get(0);
		if (item != null) {
			Media description = jooqMediaDao.fetchOneById(item.getDescription());
			List<Media> mediaList = dslContext.selectFrom(ITEM_MEDIA).where(ITEM_MEDIA.ITEM_ID.eq(id)).fetch().stream()
					.map(media -> jooqMediaDao.fetchOneById(media.getMediaId())).collect(Collectors.toList());
			ItemDto itemDto = new ItemDto(item.getId(), item.getName(), (description != null) ? description.getValue() : "", mediaList);
			
			if (Features.INTERESSES) {
				Integer amountOfLikes = dslContext.selectCount().from(INTEREST).where(INTEREST.ITEM_ID.eq(id))
						.fetchOne(0, Integer.class);
				itemDto.setAmountOfLikes(amountOfLikes);
			}
			
			return itemDto;
		}
		return null;
	}

	public Integer insertItem(ItemDto item) {
//		this.dslContext.insertInto(ITEM, ITEM.NAME, ITEM.DESCRIPTION, ITEM.TYPE)
//			.values(item.getName(), item.getDescription(), item.getType());
		return 0;
	}

	public int updateItemLikes(Integer id) {
//		try {
//			ItemDto item = this.getItem(id);
//			this.dslContext.update(ITEM)
//				.set(ITEM.AMOUNT_OF_LIKES, item.getAmountofLikes() + 1)
//				.where(ITEM.ID.equal(id))
//				.execute();
//		} catch(Exception e) { return 99; }
//		return 0;
		return 0;
	}
}
