package edu.usp.smarticmc.provedorDeConteudo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("contentProviderController")
public class RestController {
	@Autowired
	private ItemDao contentProviderItemDao;
	
	public ItemDto getItem(Integer id) {
		return contentProviderItemDao.getItem(id);
	}
}
