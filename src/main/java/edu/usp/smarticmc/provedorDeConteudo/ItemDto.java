package edu.usp.smarticmc.provedorDeConteudo;

import java.util.List;
import java.util.stream.Collectors;

import edu.usp.smarticmc.jooq.tables.pojos.Media;

public class ItemDto {
	private Integer id;
	private String name;
	private String description;
	private Integer amountOfLikes;
	private List<Media> mediaList;
	
	public ItemDto() {
		this.id = null;
		this.name = null;
		this.description = null;
		this.amountOfLikes = null;
		this.mediaList = null;
	}

	public ItemDto(Integer id, String name, String description, List<Media> mediaList) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.mediaList = mediaList;
		this.amountOfLikes = null;
		this.mediaList = this.mediaList.stream().filter(media -> !media.getValue().equals(this.description)).collect(Collectors.toList());
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getAmountOfLikes() {
		return amountOfLikes;
	}

	public void setAmountOfLikes(Integer amountOfLikes) {
		this.amountOfLikes = amountOfLikes;
	}

	public List<Media> getMediaList() {
		return mediaList;
	}

	public void setMediaList(List<Media> mediaList) {
		this.mediaList = mediaList;
	}
}
