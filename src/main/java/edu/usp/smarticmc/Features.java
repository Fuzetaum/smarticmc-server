package edu.usp.smarticmc;

public class Features {
	// Ciencia de Contexto
	public static final boolean SISTEMA_DE_RECOMENDACAO = false;
	// Informacao de Contexto
	public static final boolean PERFIL_DE_USUARIO = true;
	public static final boolean LOCALIZACAO = true;
	public static final boolean APRENDIZADO_PREVIO = false;
	public static final boolean APRENDIZADO_ADQUIRIDO = false;
	public static final boolean INTERESSES = true;
	public static final boolean RFID_DE_LOCAL = false;
	public static final boolean GPS = true;
	public static final boolean BEACON = false;
	public static final boolean AMBIENTE_DE_INSERCAO = false;
	public static final boolean RFID_DE_AMBIENTE = false;
	public static final boolean RASPBERRY_PI = false;
	public static final boolean ARDUINO = false;
	public static final boolean GIROSCOPIO = true;
	public static final boolean ACELEROMETRO = false;
	public static final boolean TERMOMETRO = false;
	// Guia do Museu
	// Visitacao Assistida
	public static final boolean ROTAS_AUTOMATIZADAS = false;
	public static final boolean SUGESTAO_DE_ROTAS = false;
	public static final boolean AVALIACAO_DE_CONHECIMENTO = false;
	public static final boolean ROTA_ATE_OBJETO = true;
	public static final boolean AUTORIA_DE_CONTEUDO = false;
	public static final boolean AUTORIA_POR_USUARIOS = false;
	public static final boolean SUGESTAO_DE_CONTEUDOS = false;
	// Midias de Conteudo
	public static final boolean TEXTO = true;
	public static final boolean IMAGEM = true;
	public static final boolean AUDIO = false;
	public static final boolean VIDEO = false;
}
