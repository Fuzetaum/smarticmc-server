package edu.usp.smarticmc.mapeadorDeRotas;

import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import edu.usp.smarticmc.Features;
import edu.usp.smarticmc.jooq.tables.pojos.MapNode;

import static edu.usp.smarticmc.jooq.tables.MapNode.MAP_NODE;

import java.util.List;
import java.util.stream.Collectors;

@Component("routePlanner")
public class RoutePlanner {
	@Autowired
	private DSLContext dslContext;
	
	public List<MapNode> getMapNodes() {
		if (Features.ROTAS_AUTOMATIZADAS) return null;
		else if (Features.SUGESTAO_DE_ROTAS) return null;
		else
			return dslContext.selectFrom(MAP_NODE).fetch().stream()
				.map(mapNode -> new MapNode(mapNode.getId(), mapNode.getLatitude(), mapNode.getLongitude(), mapNode.getIsItem()))
				.collect(Collectors.toList());
	}
}
