package edu.usp.smarticmc.mapeadorDeRotas;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import edu.usp.smarticmc.jooq.tables.pojos.MapNode;

@Component("routeMapperController")
public class RestController {
	@Autowired
	private RoutePlanner routePlanner;
	
	public List<MapNode> getMapNodes() {
		return routePlanner.getMapNodes();
	}
}
