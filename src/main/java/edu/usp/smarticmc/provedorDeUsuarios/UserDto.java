package edu.usp.smarticmc.provedorDeUsuarios;

import java.util.List;

public class UserDto {
	private String username;
	private String password;
	private String name;
	private String email;
	private List<Interest> interests;
	
	public UserDto() {
		this.username = null;
		this.email = null;
		this.password = null;
		this.name = null;
		this.interests = null;
	}

	public UserDto(String username, String email, String password, String name) {
		super();
		this.username = username;
		this.email = email;
		this.password = password;
		this.name = name;
		this.interests = interests;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Interest> getInterests() {
		return interests;
	}

	public void setInterests(List<Interest> interests) {
		this.interests = interests;
	}
}
