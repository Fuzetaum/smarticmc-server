package edu.usp.smarticmc.provedorDeUsuarios;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import edu.usp.smarticmc.Features;
import edu.usp.smarticmc.util.Logger;

@Component("userProviderController")
public class RestController {
	@Autowired
	private UserDao userProviderUserDao;
	
	public UserDto getUser(String username) {
		return userProviderUserDao.getUser(username);
	}
	
	public boolean createNewUser(String username, String password) {
		try {
			userProviderUserDao.createNewUser(username, password);
			return true;
		} catch(Exception e) {
			Logger.error("An error occurred:" + e.getMessage());
			return false;
		}
	}
	
	public boolean loginUser(String username, String password) {
		return userProviderUserDao.loginUser(username, password);
	}
	
	@SuppressWarnings("unused")
	public void logoutUser(UserVisitDto userVisit) {
		if (Features.INTERESSES && !Features.APRENDIZADO_ADQUIRIDO)
			userProviderUserDao.logoutUser(userVisit.getUsername(), userVisit.getLiked());
		else if (!Features.INTERESSES && Features.APRENDIZADO_ADQUIRIDO);
		else if (Features.INTERESSES && Features.APRENDIZADO_ADQUIRIDO);
		else
			userProviderUserDao.logoutUser(userVisit.getUsername());
	}
	
	public boolean editUser(UserDto user) {
		return userProviderUserDao.editUser(user);
	}
}
