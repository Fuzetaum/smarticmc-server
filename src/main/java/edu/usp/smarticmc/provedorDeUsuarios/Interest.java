package edu.usp.smarticmc.provedorDeUsuarios;

import edu.usp.smarticmc.provedorDeConteudo.ItemDto;

public class Interest {
	private ItemDto item;
	
	public Interest(ItemDto item) {
		this.item = item;
	}

	public ItemDto getItem() {
		return item;
	}

	public void setItem(ItemDto item) {
		this.item = item;
	}
}
