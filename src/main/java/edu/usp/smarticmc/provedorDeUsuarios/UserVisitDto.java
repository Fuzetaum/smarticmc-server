package edu.usp.smarticmc.provedorDeUsuarios;

public class UserVisitDto {
	private String username;
	private String itinerary;
	private String liked;
	
	public UserVisitDto() {}
	
	public UserVisitDto(String username, String itinerary, String liked) {
		this.username = username; this.itinerary = itinerary; this.liked = liked;
	}
	
	public String getUsername() { return this.username; }
	public void setUsername(String username) { this.username = username; }
	
	public String getItinerary() { return this.itinerary; }
	public void setItinerary(String itinerary) { this.itinerary = itinerary; }
	
	public String getLiked() { return this.liked; }
	public void setLiked(String liked) { this.liked = liked; }
	
	@Override
	public String toString() {
		return new String("username="+this.username
				+", itinerary="+this.itinerary
				+", liked="+this.liked);
	}
}
