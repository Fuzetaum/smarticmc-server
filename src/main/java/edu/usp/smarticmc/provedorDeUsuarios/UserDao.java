package edu.usp.smarticmc.provedorDeUsuarios;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.jooq.DSLContext;
import org.jooq.exception.DataAccessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import edu.usp.smarticmc.Features;
import edu.usp.smarticmc.jooq.tables.pojos.User;
import edu.usp.smarticmc.provedorDeConteudo.ItemDao;
import edu.usp.smarticmc.util.Logger;

import static edu.usp.smarticmc.jooq.tables.Interest.INTEREST;

@Component("userProviderUserDao")
public class UserDao {
	@Autowired
	private ItemDao contentProviderItemDao;
	
	@Autowired
	private edu.usp.smarticmc.jooq.tables.daos.UserDao jooqUserDao;
	
	@Autowired
	private DSLContext dslContext;
	
	public UserDto getUser(String username) {
		User user = jooqUserDao.fetchOneByUsername(username);
		if (user == null) return null;

		UserDto userDto = new UserDto(user.getUsername(), user.getEmail(), user.getPassword(), user.getName());
		
		if (Features.APRENDIZADO_PREVIO);
		
		if (Features.APRENDIZADO_ADQUIRIDO);
		
		if (Features.INTERESSES) {
			List<Interest> interestList = dslContext.selectFrom(INTEREST).fetch().stream()
					.map(interest -> new Interest(contentProviderItemDao.getItem(interest.getItemId()))).collect(Collectors.toList());
			userDto.setInterests(interestList);
		}
		
		return userDto;
	}
	
	public void createNewUser(String username, String password) throws DataAccessException {
		jooqUserDao.insert(new User(username, password, null, null));
	}
	
	public boolean loginUser(String username, String password) {
		User user = jooqUserDao.fetchOneByUsername(username);
		if (user == null) return false;
		if (!user.getPassword().equals(password)) return false;
		return true;
	}
	
	public void logoutUser(String username) {
		this.logoutUser(username, null);
	}
	
	public void logoutUser(String username, String likedItems) {
		if (likedItems == null || likedItems.isEmpty()) return;
		
		List<String> likedIdList = Arrays.asList(likedItems.split("-"));
		List<Integer> userLikedItemIds = dslContext.selectFrom(INTEREST).where(INTEREST.USERNAME.eq(username)).fetch().stream()
				.map(interest -> interest.getItemId()).collect(Collectors.toList());
		likedIdList.forEach(item -> {
			Integer itemId = Integer.parseInt(item);
			if (!userLikedItemIds.contains(itemId)) {
				Logger.info("Registering like for item: ID " + (itemId+1));
				dslContext.insertInto(INTEREST, INTEREST.USERNAME, INTEREST.ITEM_ID)
					.values(username, itemId)
					.execute();
			}
		});
	}
	
	public boolean editUser(UserDto user) {
		if (jooqUserDao.fetchOneByUsername(user.getUsername()) != null) return false;
		
		User newUser = new User();
		newUser.setUsername(user.getUsername());
		newUser.setPassword(user.getPassword());
		newUser.setEmail(user.getEmail());
		newUser.setName(user.getName());
		jooqUserDao.update(newUser);
		return true;
	}
}
