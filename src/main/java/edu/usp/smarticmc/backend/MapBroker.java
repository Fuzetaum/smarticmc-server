package edu.usp.smarticmc.backend;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.usp.smarticmc.jooq.tables.pojos.MapNode;

@Service("backendMapBroker")
public class MapBroker extends GenericBroker {
	@Autowired
	private edu.usp.smarticmc.mapeadorDeRotas.RestController routeMapperController;

	@Override
	public Object getEntity(Object identifier) { return null; }

	@Override
	public List<MapNode> getEntityList() {
		return routeMapperController.getMapNodes();
	}

}
