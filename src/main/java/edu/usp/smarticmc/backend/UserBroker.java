package edu.usp.smarticmc.backend;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.usp.smarticmc.provedorDeUsuarios.UserDto;
import edu.usp.smarticmc.provedorDeUsuarios.UserVisitDto;

@Service("backendUserBroker")
public class UserBroker extends GenericBroker {
	@Autowired
	private edu.usp.smarticmc.provedorDeUsuarios.RestController userProviderController;

	@Override
	public Object getEntity(Object identifier) {
		return userProviderController.getUser((String) identifier);
	}

	@Override
	public List<? extends Object> getEntityList() { return null; }
	
	public boolean createNewUser(String username, String password) {
		return userProviderController.createNewUser(username, password);
	}
	
	public boolean editUser(UserDto user) {
		return userProviderController.editUser(user);
	}
	
	public boolean loginUser(String username, String password) {
		return userProviderController.loginUser(username, password);
	}
	
	public void logoutUser(UserVisitDto userVisit) {
		userProviderController.logoutUser(userVisit);
	}
}
