package edu.usp.smarticmc.backend;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import edu.usp.smarticmc.jooq.tables.pojos.MapNode;
import edu.usp.smarticmc.provedorDeConteudo.ItemDto;
import edu.usp.smarticmc.provedorDeUsuarios.UserDto;
import edu.usp.smarticmc.provedorDeUsuarios.UserVisitDto;
import edu.usp.smarticmc.util.Logger;

@org.springframework.web.bind.annotation.RestController("backendRestController")
@RequestMapping(value = "/")
public class RestController {
	@Autowired
	private GenericBroker backendItemBroker;
	
	@Autowired
	private GenericBroker backendMapBroker;
	
	@Autowired
	private UserBroker backendUserBroker;
	
/* Content Provider APIs */
	
	@GetMapping("/item")
	@CrossOrigin
	public ResponseEntity<ItemDto> getItem(@RequestParam(value="id") Integer id) {
		Logger.info("Received request to get item: ID " + id);
		
		ItemDto item = (ItemDto) backendItemBroker.getEntity(id);
		
		HttpHeaders responseHeaders = new HttpHeaders();
		
		if (item == null) {
			return ResponseEntity.notFound()
					.headers(responseHeaders)
				.build();
		}
		
		return ResponseEntity.ok()
				.headers(responseHeaders)
			.body(item);
	}
	
//	@RequestMapping({"method=GET","/getall"})
//	@CrossOrigin
//	public ResponseEntity<RequestResponse> getAllItem() {
//		System.out.println("[LOG] Retrieve all items request received");
//		HttpHeaders responseHeaders = new HttpHeaders();
//	    responseHeaders.set("Content-Security-Policy", "upgrade-insecure-requests");
//		return ResponseEntity.ok()
//				.headers(responseHeaders)
//				.body(new RequestResponse(this.itemDao.getItemList()));
//	}
	
/* Route Mapper APIs */
	
	@GetMapping("/map")
	@CrossOrigin
	public ResponseEntity<List<MapNode>> getMapNodes() {
		Logger.info("Received request to get map nodes");
		HttpHeaders responseHeaders = new HttpHeaders();
//	    responseHeaders.set("Content-Security-Policy", "upgrade-insecure-requests");
		
		List<MapNode> itemList = (List<MapNode>) backendMapBroker.getEntityList();
		
		if (itemList.isEmpty()) {
			return ResponseEntity.noContent()
					.headers(responseHeaders)
				.build();
		}
		return ResponseEntity.ok()
				.headers(responseHeaders)
			.body(itemList);
	}
	
//	@GetMapping("/map/path")
//	@CrossOrigin
//	public ResponseEntity<RequestResponse> getMapPathNodes() {
//		Logger.info("Received request to get map path nodes");
//		HttpHeaders responseHeaders = new HttpHeaders();
//	    responseHeaders.set("Content-Security-Policy", "upgrade-insecure-requests");
//		return ResponseEntity.ok()
//				.headers(responseHeaders)
//				.body(new RequestResponse(mapItemDao.getPathNodeList()));
//	}
//	
//	@GetMapping("/map/item")
//	@CrossOrigin
//	public ResponseEntity<RequestResponse> getMapItemNodes() {
//		Logger.info("Received request to get map item nodes");
//		HttpHeaders responseHeaders = new HttpHeaders();
//	    responseHeaders.set("Content-Security-Policy", "upgrade-insecure-requests");
//		return ResponseEntity.ok()
//				.headers(responseHeaders)
//				.body(new RequestResponse(mapItemDao.getItemNodeList()));
//	}
	
/* User Provider APIs */
	
	@GetMapping("/user")
	@CrossOrigin
	public ResponseEntity<UserDto> getUserProfile(@RequestBody UserDto user) {
		Logger.info("Get profile request received: username=" + user.getUsername() + ", password=" + user.getPassword());
		HttpHeaders responseHeaders = new HttpHeaders();
//	    responseHeaders.set("Content-Security-Policy", "upgrade-insecure-requests");
		UserDto dbUser = (UserDto) backendUserBroker.getEntity(user.getUsername());
		
		if (dbUser == null) {
			return ResponseEntity.notFound()
					.headers(responseHeaders)
				.build();
		}
		
		return ResponseEntity.ok()
				.headers(responseHeaders)
			.body(dbUser);
	}
	
	@PostMapping("/user")
	@CrossOrigin
	public ResponseEntity<String> createUserProfile(@RequestBody UserDto user) {
		Logger.info("Create profile request received: username=" + user.getUsername() + ", password=" + user.getPassword());
		HttpHeaders responseHeaders = new HttpHeaders();
//	    responseHeaders.set("Content-Security-Policy", "upgrade-insecure-requests");
		boolean isUserCreated = backendUserBroker.createNewUser(user.getUsername(), user.getPassword());
		
		if (!isUserCreated) {
			return ResponseEntity.badRequest()
					.headers(responseHeaders)
				.build();
		}
		
		return ResponseEntity.ok()
				.headers(responseHeaders)
			.build();
	}
	
	@PutMapping("/user")
	@CrossOrigin
	public ResponseEntity<String> editUser(
			@RequestParam(value="username") String username,
			@RequestParam(value="password") String password,
			@RequestParam(value="name") String name,
			@RequestParam(value="email") String emailAddress) {
		HttpHeaders responseHeaders = new HttpHeaders();
//	    responseHeaders.set("Content-Security-Policy", "upgrade-insecure-requests");
		if (!backendUserBroker.editUser(new UserDto(username, emailAddress, password, name))) {
			return ResponseEntity.status(HttpStatus.FORBIDDEN)
					.headers(responseHeaders)
				.build();
		}
		
		return ResponseEntity.ok()
				.headers(responseHeaders)
			.build();
	}
	
	@PostMapping("/login")
	@CrossOrigin
	public ResponseEntity<String> loginUser(@RequestBody UserDto user) {
		Logger.info("User attempt to log in: username=" + user.getUsername());
		HttpHeaders responseHeaders = new HttpHeaders();
//	    responseHeaders.set("Content-Security-Policy", "upgrade-insecure-requests");
		boolean isLoginSuccess = backendUserBroker.loginUser(user.getUsername(), user.getPassword());
		
		if (!isLoginSuccess) {
			return ResponseEntity.status(HttpStatus.FORBIDDEN)
					.headers(responseHeaders)
				.build();
		}
		
		return ResponseEntity.ok()
				.headers(responseHeaders)
			.build();
	}
	
	@PostMapping("/logout")
	@CrossOrigin(origins = "https://smarticmc-dev.netlify.com,https://smarticmc.netlify.com")
	public ResponseEntity<String> userLogout(@RequestBody UserVisitDto userVisit) {
		Logger.info("Saving user session: username=" + userVisit.getUsername());
		HttpHeaders responseHeaders = new HttpHeaders();
//	    responseHeaders.set("Access-Control-Allow-Origin", "*");
//	    responseHeaders.set("Content-Security-Policy", "upgrade-insecure-requests");
		backendUserBroker.logoutUser(userVisit);
		
		return ResponseEntity.ok()
				.headers(responseHeaders)
			.build();
	}
}
