package edu.usp.smarticmc.backend;

import java.util.List;

public abstract class GenericBroker {
	public abstract Object getEntity(Object identifier);
	public abstract List<? extends Object> getEntityList();
}
