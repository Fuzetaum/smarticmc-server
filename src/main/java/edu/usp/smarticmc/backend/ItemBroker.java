package edu.usp.smarticmc.backend;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("backendItemBroker")
public class ItemBroker extends GenericBroker {
	@Autowired
	private edu.usp.smarticmc.provedorDeConteudo.RestController contentProviderController;
	
	@Override
	public Object getEntity(Object identifier) {
		return contentProviderController.getItem((Integer) identifier);
	}

	@Override
	public List<? extends Object> getEntityList() { return null; }
}
