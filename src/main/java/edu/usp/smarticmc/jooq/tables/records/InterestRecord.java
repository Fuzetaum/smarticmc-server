/*
 * This file is generated by jOOQ.
*/
package edu.usp.smarticmc.jooq.tables.records;


import edu.usp.smarticmc.jooq.tables.Interest;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record2;
import org.jooq.Row2;
import org.jooq.impl.TableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.6"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class InterestRecord extends TableRecordImpl<InterestRecord> implements Record2<String, Integer> {

    private static final long serialVersionUID = 202251469;

    /**
     * Setter for <code>interest.username</code>.
     */
    public void setUsername(String value) {
        set(0, value);
    }

    /**
     * Getter for <code>interest.username</code>.
     */
    public String getUsername() {
        return (String) get(0);
    }

    /**
     * Setter for <code>interest.item_id</code>.
     */
    public void setItemId(Integer value) {
        set(1, value);
    }

    /**
     * Getter for <code>interest.item_id</code>.
     */
    public Integer getItemId() {
        return (Integer) get(1);
    }

    // -------------------------------------------------------------------------
    // Record2 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row2<String, Integer> fieldsRow() {
        return (Row2) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row2<String, Integer> valuesRow() {
        return (Row2) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field1() {
        return Interest.INTEREST.USERNAME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field2() {
        return Interest.INTEREST.ITEM_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component1() {
        return getUsername();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer component2() {
        return getItemId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value1() {
        return getUsername();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value2() {
        return getItemId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InterestRecord value1(String value) {
        setUsername(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InterestRecord value2(Integer value) {
        setItemId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InterestRecord values(String value1, Integer value2) {
        value1(value1);
        value2(value2);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached InterestRecord
     */
    public InterestRecord() {
        super(Interest.INTEREST);
    }

    /**
     * Create a detached, initialised InterestRecord
     */
    public InterestRecord(String username, Integer itemId) {
        super(Interest.INTEREST);

        set(0, username);
        set(1, itemId);
    }
}
