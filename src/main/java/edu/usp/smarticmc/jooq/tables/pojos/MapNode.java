/*
 * This file is generated by jOOQ.
*/
package edu.usp.smarticmc.jooq.tables.pojos;


import java.io.Serializable;

import javax.annotation.Generated;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.6"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class MapNode implements Serializable {

    private static final long serialVersionUID = 1180448744;

    private Integer id;
    private Double  latitude;
    private Double  longitude;
    private Byte    isItem;

    public MapNode() {}

    public MapNode(MapNode value) {
        this.id = value.id;
        this.latitude = value.latitude;
        this.longitude = value.longitude;
        this.isItem = value.isItem;
    }

    public MapNode(
        Integer id,
        Double  latitude,
        Double  longitude,
        Byte    isItem
    ) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.isItem = isItem;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getLatitude() {
        return this.latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return this.longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Byte getIsItem() {
        return this.isItem;
    }

    public void setIsItem(Byte isItem) {
        this.isItem = isItem;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("MapNode (");

        sb.append(id);
        sb.append(", ").append(latitude);
        sb.append(", ").append(longitude);
        sb.append(", ").append(isItem);

        sb.append(")");
        return sb.toString();
    }
}
