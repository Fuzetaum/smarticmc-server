/*
 * This file is generated by jOOQ.
*/
package edu.usp.smarticmc.jooq.tables.pojos;


import java.io.Serializable;

import javax.annotation.Generated;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.6"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Media implements Serializable {

    private static final long serialVersionUID = 1448722011;

    private Integer id;
    private String  type;
    private String  value;

    public Media() {}

    public Media(Media value) {
        this.id = value.id;
        this.type = value.type;
        this.value = value.value;
    }

    public Media(
        Integer id,
        String  type,
        String  value
    ) {
        this.id = id;
        this.type = type;
        this.value = value;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Media (");

        sb.append(id);
        sb.append(", ").append(type);
        sb.append(", ").append(value);

        sb.append(")");
        return sb.toString();
    }
}
