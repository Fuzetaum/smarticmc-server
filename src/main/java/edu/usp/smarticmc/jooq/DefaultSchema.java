/*
 * This file is generated by jOOQ.
*/
package edu.usp.smarticmc.jooq;


import edu.usp.smarticmc.jooq.tables.Interest;
import edu.usp.smarticmc.jooq.tables.Item;
import edu.usp.smarticmc.jooq.tables.ItemMedia;
import edu.usp.smarticmc.jooq.tables.MapNode;
import edu.usp.smarticmc.jooq.tables.MapPath;
import edu.usp.smarticmc.jooq.tables.Media;
import edu.usp.smarticmc.jooq.tables.User;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Catalog;
import org.jooq.Table;
import org.jooq.impl.SchemaImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.6"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class DefaultSchema extends SchemaImpl {

    private static final long serialVersionUID = 188932087;

    /**
     * The reference instance of <code></code>
     */
    public static final DefaultSchema DEFAULT_SCHEMA = new DefaultSchema();

    /**
     * The table <code>interest</code>.
     */
    public final Interest INTEREST = edu.usp.smarticmc.jooq.tables.Interest.INTEREST;

    /**
     * The table <code>item</code>.
     */
    public final Item ITEM = edu.usp.smarticmc.jooq.tables.Item.ITEM;

    /**
     * The table <code>item_media</code>.
     */
    public final ItemMedia ITEM_MEDIA = edu.usp.smarticmc.jooq.tables.ItemMedia.ITEM_MEDIA;

    /**
     * The table <code>map_node</code>.
     */
    public final MapNode MAP_NODE = edu.usp.smarticmc.jooq.tables.MapNode.MAP_NODE;

    /**
     * The table <code>map_path</code>.
     */
    public final MapPath MAP_PATH = edu.usp.smarticmc.jooq.tables.MapPath.MAP_PATH;

    /**
     * The table <code>media</code>.
     */
    public final Media MEDIA = edu.usp.smarticmc.jooq.tables.Media.MEDIA;

    /**
     * The table <code>user</code>.
     */
    public final User USER = edu.usp.smarticmc.jooq.tables.User.USER;

    /**
     * No further instances allowed
     */
    private DefaultSchema() {
        super("", null);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public Catalog getCatalog() {
        return DefaultCatalog.DEFAULT_CATALOG;
    }

    @Override
    public final List<Table<?>> getTables() {
        List result = new ArrayList();
        result.addAll(getTables0());
        return result;
    }

    private final List<Table<?>> getTables0() {
        return Arrays.<Table<?>>asList(
            Interest.INTEREST,
            Item.ITEM,
            ItemMedia.ITEM_MEDIA,
            MapNode.MAP_NODE,
            MapPath.MAP_PATH,
            Media.MEDIA,
            User.USER);
    }
}
