package edu.usp.smarticmc.configuration;

import javax.sql.DataSource;

import org.jooq.SQLDialect;
import org.jooq.impl.DataSourceConnectionProvider;
import org.jooq.impl.DefaultConfiguration;
import org.jooq.impl.DefaultDSLContext;
import org.jooq.impl.DefaultExecuteListenerProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jooq.JooqExceptionTranslator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import edu.usp.smarticmc.jooq.tables.daos.ItemDao;
import edu.usp.smarticmc.jooq.tables.daos.MapNodeDao;
import edu.usp.smarticmc.jooq.tables.daos.MediaDao;
import edu.usp.smarticmc.jooq.tables.daos.UserDao;

@EnableTransactionManagement
@Configuration
public class JooqConfiguration {
	@Qualifier("dataSource")
    @Autowired
    private DataSource dataSource;

    @Value(value = "${spring.datasource.username}")
    private String username;

    @Value(value = "${spring.datasource.password}")
    private String password;

    @Value(value = "${spring.datasource.url}")
    private String url;

    @Value(value = "${spring.jooq.sql-dialect}")
    private SQLDialect sqlDialect;

    @Bean
    public DataSourceConnectionProvider connectionProvider() {
        return new DataSourceConnectionProvider(dataSource);
    }

    @Bean(value = "dslContext")
    public DefaultDSLContext dsl() {
        return new DefaultDSLContext(configuration());
    }
    
    @Bean(value = "jooqItemDao")
    public ItemDao itemDao() { return new ItemDao(configuration()); }
    
    @Bean(value = "jooqMediaDao")
    public MediaDao mediaDao() { return new MediaDao(configuration()); }
    
    @Bean(value = "jooqMapNodeDao")
    public MapNodeDao mapNodeDao() { return new MapNodeDao(configuration()); }
    
    @Bean(value = "jooqUserDao")
    public UserDao userDao() { return new UserDao(configuration()); }

    public DefaultConfiguration configuration() {
        DefaultConfiguration jooqConfiguration = new DefaultConfiguration();
        jooqConfiguration.set(connectionProvider());
        jooqConfiguration.set(new DefaultExecuteListenerProvider(new JooqExceptionTranslator()));
        jooqConfiguration.set(sqlDialect);
        return jooqConfiguration;
    }
}
