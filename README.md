#Environment Variables#

These are the environment variables that must be set, in order to the system to properly start:

**DATASOURCE_URL** = jdbc:mysql://10.62.8.4:3306/ricardofuzeto

**DATASOURCE_USERNAME** = ricardofuzeto

**DATASOURCE_PASSWORD** = 5m4r71Cmc

**SSL_KEYSTORE_TYPE** = PKCS12

**SSL_KEYSTORE** = keystore.p12

**SSL_KEYSTORE_PASSWORD** = DATASOURCE_PASSWORD

**SSL_KEY_ALIAS** = smarticmc